/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author luism
 */
public class cuentaAhorro {
    private int numcuenta;
    private String feap,nomban;
    private float porcrend,saldo;
    private datosCliente datoscli;
    
    public cuentaAhorro(){
        this.numcuenta=numcuenta;
        this.feap=feap;
        this.nomban=nomban;
        this.porcrend=porcrend;
        this.saldo=saldo;
        this.datoscli=datoscli;
    }
    
    public cuentaAhorro(int numcuenta,String feap,String nomban,float porcrend,float saldo,datosCliente datoscli){
        this.numcuenta=0;
        this.feap="";
        this.nomban="";
        this.porcrend=0.0f;
        this.saldo=0.0f;
    }
    
    public cuentaAhorro(cuentaAhorro obj){
        this.numcuenta=obj.numcuenta;
        this.feap=obj.feap;
        this.nomban=obj.nomban;
        this.porcrend=obj.porcrend;
        this.saldo=obj.saldo;
        this.datoscli=obj.datoscli;
    }

    public void Depositar(float cant){
        
    }
    
    public boolean Retirar(float cant){
        return false;
        //return true;
        
    }
    
    public float calcularReditos(){
        return 0;
        
    }
    
    
    public int getNumcuenta() {
        return numcuenta;
    }

    public void setNumcuenta(int numcuenta) {
        this.numcuenta = numcuenta;
    }

    public String getFeap() {
        return feap;
    }

    public void setFeap(String feap) {
        this.feap = feap;
    }

    public String getNomban() {
        return nomban;
    }

    public void setNomban(String nomban) {
        this.nomban = nomban;
    }

    public float getPorcrend() {
        return porcrend;
    }

    public void setPorcrend(float porcrend) {
        this.porcrend = porcrend;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public datosCliente getDatoscli() {
        return datoscli;
    }

    public void setDatoscli(datosCliente datoscli) {
        this.datoscli = datoscli;
    }
    
}
