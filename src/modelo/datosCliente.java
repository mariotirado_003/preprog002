/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author luism
 */
public class datosCliente {
    private String nomcliente,fecnac,domi,sexo;
    
    public datosCliente(){
        this.nomcliente=nomcliente;
        this.fecnac=fecnac;
        this.domi=domi;
        this.sexo=sexo;
    }
    
    public datosCliente(String nomcliente,String fecnac,String domi,String sexo){
        this.nomcliente="";
        this.fecnac="";
        this.domi="";
        this.sexo="";
    }
    
    public datosCliente(datosCliente obj){
        this.nomcliente=obj.nomcliente;
        this.fecnac=obj.fecnac;
        this.domi=obj.domi;
        this.sexo=obj.sexo;
    }

    public String getNomcliente() {
        return nomcliente;
    }

    public void setNomcliente(String nomcliente) {
        this.nomcliente = nomcliente;
    }

    public String getFecnac() {
        return fecnac;
    }

    public void setFecnac(String fecnac) {
        this.fecnac = fecnac;
    }

    public String getDomi() {
        return domi;
    }

    public void setDomi(String domi) {
        this.domi = domi;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    
    
}
